﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Topshelf;

namespace PabloService.Service
{
    class WinService
    {
        bool _enabled = true;

        public bool Start(HostControl hostControl)
        {
            new Task(() =>
            {
                PabloServiceCore.Program.KeepAlive = () => { return _enabled; };
                PabloServiceCore.Program.Main(null);
            }).Start();
            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            _enabled = false;
            return true;
        }

        public bool Pause(HostControl hostControl)
        {
            return Stop(hostControl);
        }

        public bool Continue(HostControl hostControl)
        {
            return Start(hostControl);
        }

        public bool Shutdown(HostControl hostControl)
        {
            return Stop(hostControl);
        }

    }
}
