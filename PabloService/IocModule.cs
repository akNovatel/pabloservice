﻿using System;

namespace PabloService
{
    class IocModule : Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
            Bind<Service.WinService>().ToSelf();
        }
    }
}

