﻿using System;
using System.IO;
using Topshelf;
using Topshelf.Ninject;

namespace PabloService
{
    class Program
    {
        static void Main(string[] args)
        {
            // This will ensure that future calls to Directory.GetCurrentDirectory()
            // returns the actual executable directory and not something like C:\Windows\System32 
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            // Specify the base name, display name and description for the service, as it is registered in the services control manager.
            // This information is visible through the Windows Service Monitor
            const string serviceName = "Pablo";
            const string displayName = "Pablo Service";
            const string description = "A .NET Windows Service of P.A.B.L.O. System";

            HostFactory.Run(x =>
            {
                x.UseNinject(new IocModule());

                x.Service<Service.WinService>(sc =>
                {
                    sc.ConstructUsingNinject();

                    // the start and stop methods for the service
                    sc.WhenStarted((s, hostControl) => s.Start(hostControl));
                    sc.WhenStopped((s, hostControl) => s.Stop(hostControl));

                    // optional pause/continue methods if used
                    sc.WhenPaused((s, hostControl) => s.Pause(hostControl));
                    sc.WhenContinued((s, hostControl) => s.Continue(hostControl));

                    // optional, when shutdown is supported
                    sc.WhenShutdown((s, hostControl) => s.Shutdown(hostControl));
                });

                //=> Service Identity

                x.RunAsLocalSystem();

                x.SetDescription(description);
                x.SetDisplayName(displayName);
                x.SetServiceName(serviceName);

            });
        }
    }
}
