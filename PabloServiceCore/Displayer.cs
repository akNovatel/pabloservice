﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace PabloServiceCore
{
    public class Displayer
    {
        Timer _timer = new Timer();
        View.IView _view;

        public Displayer(View.IView view)
        {
            _view = view;

            _timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            _timer.Interval = 500;
            _timer.Enabled = true;
        }

        void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            _view.Draw();
        }
    }
}
