﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore
{
    class DBControl
    {
        DBAccess.IDBAccess _dbAccess;
        public DBControl(DBAccess.IDBAccess dbAccess)
        {
            _dbAccess = dbAccess;
        }

        public void CreateTables()
        {
            //actual
            if (!_dbAccess.Exists("tag"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating TAG table");
                _dbAccess.Create("tag", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },

                    {"tagid", DBAccess.ColumnType.INT },
                    {"tagtime", DBAccess.ColumnType.INT }
                });
            }
            if (!_dbAccess.Exists("gps"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating GPS table");
                _dbAccess.Create("gps", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },

                    {"latitude", DBAccess.ColumnType.VARCHAR },
                    {"longtitude", DBAccess.ColumnType.VARCHAR }
                });
            }
            if (!_dbAccess.Exists("alarm"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating ALARM table");
                _dbAccess.Create("alarm", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },

                    {"state", DBAccess.ColumnType.INT }
                });
            }

            //historical
            if (!_dbAccess.Exists("past_tag"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating TAG history table");
                _dbAccess.Create("past_tag", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },

                    {"tagid", DBAccess.ColumnType.INT },
                    {"tagtime", DBAccess.ColumnType.INT }
                });
            }
            if (!_dbAccess.Exists("past_gps"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating GPS history table");
                _dbAccess.Create("past_gps", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },

                    {"latitude", DBAccess.ColumnType.VARCHAR },
                    {"longtitude", DBAccess.ColumnType.VARCHAR }
                });
            }
            if (!_dbAccess.Exists("past_alarm"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating ALARM history table");
                _dbAccess.Create("past_alarm", new Dictionary<string, DBAccess.ColumnType> {
                    {"radioid", DBAccess.ColumnType.INT },
                    {"savetime", DBAccess.ColumnType.INT },
                });
            }

            //aliases
            if (!_dbAccess.Exists("aliases"))
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Creating alias table");
                _dbAccess.Create("aliases", new Dictionary<string, DBAccess.ColumnType> {
                    {"number", DBAccess.ColumnType.INT },
                    {"type", DBAccess.ColumnType.VARCHAR },
                    {"alias", DBAccess.ColumnType.VARCHAR }
                });
            }
        }

        public void CreateVirtualTables()
        {
            string radioidTable = "SELECT radioid from tag UNION SELECT radioid FROM gps UNION SELECT radioid FROM alarm";
            //string lastValid = "SELECT CASE WHEN IFNULL(t.savetime,0) >= IFNULL(g.savetime,0) THEN 'tag' ELSE 'gps' END FROM tag t JOIN gps g ON t.radioid = g.radioid";
            _dbAccess.AddVirtual("trace", $"SELECT ABS(RANDOM() % 999999) id, d.radioid, t.tagid, t.tagtime, g.latitude gpslat, g.longtitude gpslng, a.state alarm, MAX(IFNULL(t.savetime,0), IFNULL(g.savetime,0), IFNULL(a.savetime,0)) savetime, CASE WHEN IFNULL(t.savetime,0) >= IFNULL(g.savetime,0) THEN 'tag' ELSE 'gps' END lastvalid FROM ({radioidTable}) d LEFT JOIN tag t ON d.radioid = t.radioid LEFT JOIN gps g ON d.radioid = g.radioid LEFT JOIN alarm a ON d.radioid = a.radioid");
        }
    }
}
