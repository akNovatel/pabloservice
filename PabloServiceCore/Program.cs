﻿using Newtonsoft.Json;
using PabloServiceCore.ConfigAccess;
using PabloServiceCore.Serial;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace PabloServiceCore
{
    public class Program
    {
        public static Func<bool> KeepAlive { get; set; }

        public static void Main(string[] args)
        {
            Logger.GetInstance().Add(Logger.Priority.INFO, "Application started");

            IConfigAccess config = new YAMLAccess("PabloService.yml");
            Logger.GetInstance().Setup(config);
            Logger.GetInstance().Add(Logger.Priority.DEBUG, "Configuration loaded");

            //Application Killer
            new Task(() => {
                while (KeepAlive())
                {
                    Thread.Sleep(2000);
                }

                config.SaveIfExtended();
                Logger.GetInstance().Add(Logger.Priority.INFO, "Application closed");
                Environment.Exit(0);
            }).Start();

            DBAccess.IDBAccess dbAccess = new DBAccess.SQLiteAccess("data.sqlite");

            //tworzenie tabel
            DBControl dbControl = new DBControl(dbAccess);
            dbControl.CreateTables();
            dbControl.CreateVirtualTables();

            Logger.GetInstance().Add(Logger.Priority.DEBUG, $"Displayer starting...");
            //Displayer displayer = new Displayer(new View.DBView(dbAccess));
            Displayer displayer = new Displayer(new View.LogView());
            Logger.GetInstance().Add(Logger.Priority.DEBUG, $"Displayer started");

            Logger.GetInstance().Add(Logger.Priority.DEBUG, $"HTTP Server starting...");
            Server.HTTPServer server = new Server.HTTPServer(config);
            server.AddHandler("db", new Server.DBReqHandler(dbAccess));
            Logger.GetInstance().Add(Logger.Priority.INFO, $"HTTP Server started on port {server.Port}");

            //UWAGA: HyteraControler blokuje wykonywanie kodu aż do uzyskania poprawnego połączenia z bazówką.
            Logger.GetInstance().Add(Logger.Priority.DEBUG, $"Hytera Controller starting...");
            HyteraControler hyteraControler = new HyteraControler(config, dbAccess);
            Logger.GetInstance().Add(Logger.Priority.INFO, $"Hytera Controller started");

            Thread.Sleep(Timeout.Infinite);
        }
    }
}
