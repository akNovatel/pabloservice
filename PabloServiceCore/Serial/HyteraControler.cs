﻿using PabloServiceCore.ConfigAccess;
using PabloServiceCore.Serial.Message;
using PabloServiceCore.Serial.Packet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial
{
    class HyteraControler
    {
        HyteraCom _hyteraCom = null;
        DBAccess.IDBAccess _dbAccess;

        bool _configAck;

        System.Timers.Timer _configSetter = null;

        public HyteraControler(IConfigAccess config, DBAccess.IDBAccess dbAccess)
        {
            _dbAccess = dbAccess;
            _hyteraCom = new HyteraCom(config);

            _hyteraCom.Connect(() => {
                //Console.WriteLine("Połączono. Wysyłam konfigurację");
                Logger.GetInstance().Add(Logger.Priority.INFO, "Hytera Controller connected.");
                //_hyteraCom.SendData(new RCP(0x00c6)); //Power Up Check Request
                //_hyteraCom.SendData(new RCP(0x10d0, new byte[] { 0x01 })); //Text Message Notification Request = only the peripheral equipment (request party) will be notified
            }); //connection request

            _hyteraCom.ListenData(Listener);

            _configSetter = new System.Timers.Timer(10000); //10s
            _configSetter.Elapsed += ConfigSetter;
            _configSetter.Start();
        }

        void ConfigSetter(object o, EventArgs e)
        {
            _configAck = false;

            //_hyteraCom.SendData(new RCP(0x10d0, new byte[] { 0x01 })); //Text Message Notification Request = only the peripheral equipment (request party) will be notified
            //Logger.GetInstance().Add("Text Message Notification Request was sent");

            _hyteraCom.SendData(new RCP(0x10c9, new byte[] { 0x02, 0x00, 0x01, 0x01, 0x01 })); //Broadcast Status Configuration Request Size[1]+Config[2]*N
            Logger.GetInstance().Add(Logger.Priority.DEBUG, "Status Broadcast Configuration Request was sent");

            //oczekiwanie na Status Broadcast Configuration Reply
            new Task(() => {
                Thread.Sleep(2500); //2.5s
                if (_configAck == false)
                {
                    Logger.GetInstance().Add(Logger.Priority.ERROR, "No Status Broadcast Configuration Reply received");

                    //_errorHandler();
                    _hyteraCom.Reconnect();
                }
            }).Start();
        }

        void Listener(IPacket packet)
        {
            HDAP hdapPacket = packet as HDAP;
            Logger.GetInstance().Add("Received HDAP", hdapPacket.Data);

            if (packet is RCP)
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "HDAP is RCP");
                switch (hdapPacket.Opcode)
                {
                    case 0xb844:
                        //tu obsługa alarmów
                        Logger.GetInstance().Add("RCP is Broadcast Receive Status", hdapPacket.Payload);
                        if (hdapPacket.Payload.Count() == 12)
                        {
                            UInt16 radioid = (UInt16)((hdapPacket.Payload[9] << 8) | hdapPacket.Payload[8]);

                            if (radioid > 0)
                            {
                                Logger.GetInstance().Add(Logger.Priority.INFO, $"Received alarm for {radioid}");
                                var data = new Dictionary<string, object>();
                                data.Add("radioid", radioid);
                                data.Add("savetime", UnixTime.Get());
                                _dbAccess.Put("past_alarm", data);

                                data.Add("state", "1");
                                _dbAccess.AddCriteria("radioid", radioid);
                                _dbAccess.UpdateExisting("alarm", data);
                            }
                        }
                        break;
                    case 0x80c9:
                        Logger.GetInstance().Add("RCP is Status Broadcast Configuration Reply", hdapPacket.Payload);
                        _configAck = true;
                        break;
                    case 0x80d0:
                        Logger.GetInstance().Add("RCP is Text Message Notification Reply", hdapPacket.Payload);
                        break;
                    case 0x80c6:
                        Logger.GetInstance().Add("RCP is Power Up Check Reply", hdapPacket.Payload);
                        break;
                    default:
                        break;
                }
            }
            else if (packet is TMP)
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "HDAP is TMP");
                if (hdapPacket.Opcode == 0x80a1)
                {
                    Logger.GetInstance().Add("TMP is PrivateMsg", hdapPacket.Payload);
                    var tag = new TAG(hdapPacket.Payload);
                    if (tag.IsDataValid)
                    {
                        Logger.GetInstance().Add(Logger.Priority.INFO, $"PrivateMsg is valid TAG: {tag.RadioID} at {tag.TagID}");
                        var data = new Dictionary<string, object>();
                        data.Add("radioid", tag.RadioID);
                        data.Add("tagid", tag.TagID);
                        data.Add("savetime", UnixTime.Get());
                        data.Add("tagtime", UnixTime.Get());
                        _dbAccess.Put("past_tag", data);

                        _dbAccess.AddCriteria("radioid", tag.RadioID);
                        _dbAccess.UpdateExisting("tag", data);
                    }
                }
            }
            else if (packet is LP)
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "HDAP is LP");
                if (hdapPacket.Opcode == 0xc003)
                {
                    Logger.GetInstance().Add("LP is GPS Report", hdapPacket.Payload);
                    var gps = new GPS(hdapPacket.Payload);
                    if (gps.IsDataValid)
                    {
                        if (gps.Latitude > 0 && gps.Longtitude > 0)
                        {
                            Logger.GetInstance().Add(Logger.Priority.INFO, $"GPS Report is valid GPS: {gps.RadioID} at {gps.Latitude} {gps.Longtitude}");
                            var data = new Dictionary<string, object>();
                            data.Add("radioid", gps.RadioID);
                            data.Add("latitude", gps.Latitude);
                            data.Add("longtitude", gps.Longtitude);
                            data.Add("savetime", UnixTime.Get());
                            _dbAccess.Put("past_gps", data);

                            _dbAccess.AddCriteria("radioid", gps.RadioID);
                            _dbAccess.UpdateExisting("gps", data);
                        }
                        else
                        {
                            Logger.GetInstance().Add(Logger.Priority.WARNING, $"GPS Report for {gps.RadioID} has invalid coordinates");
                        }
                    }
                }
            }
            else
            {
                Logger.GetInstance().Add(Logger.Priority.WARNING, "HDAP is unknown");
            }
        }

        //public void Dispose()
        //{
        //    if (_configSetter != null)
        //    {
        //        _configSetter.Stop();
        //        _configSetter.Close();
        //        _configSetter.Dispose();
        //    }

        //    if (_hyteraCom != null)
        //    {
        //        _hyteraCom.Dispose();
        //    }
        //}
    }
}
