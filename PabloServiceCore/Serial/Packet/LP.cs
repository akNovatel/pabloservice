﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public class LP : HDAP
    {
        const byte hdrID = 0x08;
        const ENDIAN endian = ENDIAN.BIG;

        public LP(byte[] data) : base(hdrID, endian, data) { }
        public LP(UInt16 opcode, byte[] payload = null) : base(hdrID, endian, opcode, payload) { }
    }
}
