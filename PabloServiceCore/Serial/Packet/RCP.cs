﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public class RCP : HDAP
    {
        const byte hdrID = 0x02;
        const ENDIAN endian = ENDIAN.LITTLE;

        public RCP(byte[] data) : base(hdrID, endian, data) { }
        public RCP(UInt16 opcode, byte[] payload = null) : base(hdrID, endian, opcode, payload) { }
    }
}
