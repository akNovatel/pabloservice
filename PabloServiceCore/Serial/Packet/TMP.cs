﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public class TMP : HDAP
    {
        const byte hdrID = 0x09;
        const ENDIAN endian = ENDIAN.BIG;

        public TMP(byte[] data) : base(hdrID, endian, data) { }
        public TMP(UInt16 opcode, byte[] payload = null) : base(hdrID, endian, opcode, payload) { }
    }
}
