﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public interface IPacket
    {
        byte[] Data { get; }
        bool IsDataValid { get; }
    }
}
