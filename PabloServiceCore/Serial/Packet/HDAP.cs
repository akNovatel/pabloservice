﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public abstract class HDAP : IPacket
    {
        protected byte _rawChecksum;
        public UInt16 Opcode { get; private set; }
        //protected UInt16 _opcode;
        protected UInt16 _rawLength;
        byte _hdrID, _rawHdrID;

        public enum ENDIAN
        {
            BIG,
            LITTLE
        }
        ENDIAN _endianmode;

        public HDAP(byte hdrID, ENDIAN endianmode, UInt16 opcode, byte[] payload = null)
        {
            _hdrID = hdrID;
            _endianmode = endianmode;
            Opcode = opcode;
            if (payload == null)
            {
                Payload = new byte[] { };
            }
            else
            {
                Payload = payload;
            }
        }

        public HDAP(byte hdrID, ENDIAN endianmode, byte[] data)
        {
            _hdrID = hdrID;
            _endianmode = endianmode;
            Data = data;
        }

        public bool IsDataValid
        {
            get
            {
                bool result = true;
                result &= _hdrID == _rawHdrID;
                result &= _rawLength == Length;
                result &= _rawChecksum == Checksum;
                return result;
            }
        }

        public byte[] Data
        {
            get
            {
                switch (_endianmode)
                {
                    default:
                    case ENDIAN.BIG:
                        return (new byte[]
                        {
                            _hdrID,

                            (byte)(Opcode >> 8),
                            (byte)(Opcode & 0xff),

                            (byte)(Length >> 8),
                            (byte)(Length & 0xff)
                        }).Concat(Payload).Concat(new byte[]
                        {
                            Checksum,
                            0x03
                        }).ToArray();
                    case ENDIAN.LITTLE:
                        return (new byte[]
                        {
                            _hdrID,

                            (byte)(Opcode & 0xff),
                            (byte)(Opcode >> 8),

                            (byte)(Length & 0xff),
                            (byte)(Length >> 8)
                        }).Concat(Payload).Concat(new byte[]
                        {
                            Checksum,
                            0x03
                        }).ToArray();
                }
            }

            private set
            {
                _rawHdrID = value[0];
                switch (_endianmode)
                {
                    default:
                    case ENDIAN.BIG:
                        Opcode = (UInt16)((value[1] << 8) | value[2]);
                        _rawLength = (UInt16)((value[3] << 8) | value[4]);
                        break;
                    case ENDIAN.LITTLE:
                        Opcode = (UInt16)((value[2] << 8) | value[1]);
                        _rawLength = (UInt16)((value[4] << 8) | value[3]);
                        break;
                }
                if (7 + _rawLength <= value.Length)
                {
                    _rawChecksum = value[value.Length - 2];
                    Payload = value.Skip(5).Take(_rawLength).ToArray();
                    //Payload = value.Skip(4).Take(_rawLength).ToArray();
                }
                else
                {
                    _rawChecksum = 0;
                    Payload = new byte[] { };
                }
            }
        }

        public byte[] Payload { get; protected set; }

        public UInt16 Length
        {
            get
            {
                return (UInt16)Payload.Length;
            }
            private set
            {
                if (value > Payload.Length)
                {
                    _rawLength = 0;
                }
                else
                {
                    _rawLength = value;
                }
            }
        }

        public byte Checksum
        {
            get
            {
                int sum = 0;

                sum += Opcode >> 8;
                sum += Opcode & 0xff;
                sum += Length >> 8;
                sum += Length & 0xff;

                foreach (var item in Payload)
                {
                    sum += item;
                }

                sum = 0xff - sum;
                sum += 0x33;

                return (byte)sum;
            }
            private set
            {
                _rawChecksum = value;
            }
        }
    }
}
