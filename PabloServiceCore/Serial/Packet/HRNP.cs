﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Packet
{
    public class HRNP : IPacket
    {
        public enum OPCODE
        {
            INVALID,
            CONNECT,
            ACCEPT,
            REJECT,
            CLOSE,
            CLOSE_ACK,
            DATA,
            DATA_ACK
        }

        UInt16 _rawChecksum;
        UInt16 _rawLength;
        byte _opcode;

        public HRNP(OPCODE opcode, byte srcID, byte destID, byte[] payload = null, UInt16 pn = 0, byte block = 0, byte version = 3)
        {
            Opcode = opcode;
            Version = version;
            Block = block;
            SrcID = srcID;
            DestID = destID;
            PacketNumber = pn;
            if (payload == null)
            {
                Payload = new byte[] { };
            }
            else
            {
                Payload = payload;
            }
        }

        public HRNP(byte[] data)
        {
            Data = data;
        }

        public bool IsDataValid
        {
            get
            {
                bool result = true;
                result &= _rawLength == Length;
                result &= _rawChecksum == Checksum;
                result &= Opcode != OPCODE.INVALID;
                return result;
            }
        }

        static public bool PatternMatch(byte[] data)
        {
            if (data.Length >= 12)
            {
                return (data[0] == 0x7e
                    && (data[1] == 0x04 || data[1] == 0x03)
                    && ((data[8] << 8) | data[9]) >= 12);
            }
            return false;
        }

        public byte Version { get; private set; }
        public byte Block { get; private set; }
        public byte SrcID { get; private set; }
        public byte DestID { get; private set; }
        public UInt16 PacketNumber { get; private set; }
        public byte[] Payload { get; private set; }

        public OPCODE Opcode
        {
            get
            {
                switch (_opcode)
                {
                    case 0xfe:
                        return OPCODE.CONNECT;
                    case 0xfd:
                        return OPCODE.ACCEPT;
                    case 0xfc:
                        return OPCODE.REJECT;
                    case 0xfb:
                        return OPCODE.CLOSE;
                    case 0xfa:
                        return OPCODE.CLOSE_ACK;
                    case 0x00:
                        return OPCODE.DATA;
                    case 0x10:
                        return OPCODE.DATA_ACK;
                    default:
                        break;
                }
                return OPCODE.INVALID;
            }
            private set
            {
                switch (value)
                {
                    case OPCODE.INVALID:
                        _opcode = 0xff;
                        break;
                    case OPCODE.CONNECT:
                        _opcode = 0xfe;
                        break;
                    case OPCODE.ACCEPT:
                        _opcode = 0xfd;
                        break;
                    case OPCODE.REJECT:
                        _opcode = 0xfc;
                        break;
                    case OPCODE.CLOSE:
                        _opcode = 0xfb;
                        break;
                    case OPCODE.CLOSE_ACK:
                        _opcode = 0xfa;
                        break;
                    case OPCODE.DATA:
                        _opcode = 0x00;
                        break;
                    case OPCODE.DATA_ACK:
                        _opcode = 0x10;
                        break;
                    default:
                        break;
                }
            }
        }

        public UInt16 Length {
            get
            {
                return (UInt16)(Payload.Length + 12);
            }
            private set
            {
                _rawLength = value;
            }
        }
        public UInt16 Checksum {
            get
            {
                int sum = 0;

                //header
                sum += (Version << 8) | 0x7e;
                sum += (_opcode << 8) | Block;
                sum += (DestID << 8) | SrcID;

                sum += ((PacketNumber << 8) & 0xff00) | ((PacketNumber >> 8) & 0xff);
                sum += ((Length << 8) & 0xff00) | ((Length >> 8) & 0xff);

                //payload
                for (int i = 0; i < Payload.Length; i++)
                {
                    if (i % 2 == 0)
                    {
                        //high
                        sum += Payload[i];
                    }
                    else
                    {
                        //low
                        sum += Payload[i] << 8;
                    }
                }

                while(sum > 0xffff)
                {
                    sum = (sum & 0xffff) + (sum >> 16);
                }
                sum = ((sum << 8) | (sum >> 8)) & 0xffff;

                return (UInt16)(0xffff - sum);
            }
            private set
            {
                _rawChecksum = value;
            }
        }

        public byte[] Data
        {
            get
            {
                return (new byte[] 
                {
                    0x7e,
                    Version,
                    Block,
                    _opcode,
                    SrcID,
                    DestID,
                    (byte)(PacketNumber >> 8),
                    (byte)(PacketNumber & 0xff),
                    (byte)(Length >> 8),
                    (byte)(Length & 0xff),
                    (byte)(Checksum >> 8),
                    (byte)(Checksum & 0xff),
                }).Concat(Payload).ToArray();
            }

            private set
            {
                Version = value[1];
                Block = value[2];
                _opcode = value[3];
                SrcID = value[4];
                DestID = value[5];
                PacketNumber = (UInt16)((value[6] << 8) | value[7]);

                _rawLength = (UInt16)((value[8] << 8) | value[9]);
                _rawChecksum = (UInt16)((value[10] << 8) | value[11]);

                Payload = value.Skip(12).ToArray();
            }
        }
    }
}
