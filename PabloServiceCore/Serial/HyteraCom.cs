﻿using PabloServiceCore.ConfigAccess;
using PabloServiceCore.Serial.Packet;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial
{
    public class HyteraCom
    {
        const byte _sourceID = 0x20;
        const byte _destinationID = 0x10;

        const int _recheckTimes = 4;
        const int _recheckInterval = 128;

        UInt16 _packetNumber = 1;

        Port.IPort _port = null;
        List<byte> _serialBuffer = new List<byte>();

        ConcurrentQueue<HRNP> _rxQueue = new ConcurrentQueue<HRNP>();
        //List<HRNP> _rxQueue = new List<HRNP>();

        Action _onConnected;
        bool _connectBlocker;

        System.Timers.Timer _dataReader = null;

        public HyteraCom(IConfigAccess config)
        {
            _port = new Port.UARTPort(config);
            _port.ListenPort(ByteRead);
        }

        void SendConnect()
        {
            //Console.WriteLine("Wysyłam CONNECT");
            SendData(new HRNP(HRNP.OPCODE.CONNECT, _sourceID, _destinationID));
        }

        public void Connect(Action onConnected)
        {
            if (_connectBlocker)
            {
                return;
            }
            _connectBlocker = true;

            _onConnected = onConnected;

            SendConnect();

            HRNP hrnpPacket;
            int timeout = _recheckTimes;
            while (true)
            {
                if (_rxQueue.TryDequeue(out hrnpPacket))
                {
                    if (hrnpPacket.Opcode == HRNP.OPCODE.ACCEPT)
                    {
                        break;
                    }
                }
                Thread.Sleep(_recheckInterval);
                if (timeout-- == 0)
                {
                    SendConnect();
                    timeout = _recheckTimes;
                }
            }
            onConnected();
            _connectBlocker = false;
        }

        public void Reconnect()
        {
            _packetNumber = 1;
            _serialBuffer.Clear();

            //SendData(new HRNP(HRNP.OPCODE.CLOSE, _sourceID, _destinationID));
            Connect(_onConnected);
        }

        public void ListenData(Action<HDAP> callback)
        {
            _dataReader = new System.Timers.Timer(_recheckInterval);
            _dataReader.Elapsed += (s, e) => {
                HRNP hrnpPacket;

                if (_rxQueue.TryDequeue(out hrnpPacket))
                {
                    Logger.GetInstance().Add("!Received HRNP", hrnpPacket.Data);

                    switch (hrnpPacket.Opcode)
                    {
                        case HRNP.OPCODE.INVALID:
                            Logger.GetInstance().Add(Logger.Priority.WARNING, "HRNP is INVALID");
                            break;
                        case HRNP.OPCODE.CONNECT:
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "HRNP is CONNECT");
                            break;
                        case HRNP.OPCODE.ACCEPT:
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "HRNP is ACCEPT");
                            break;
                        case HRNP.OPCODE.REJECT:
                            Logger.GetInstance().Add(Logger.Priority.WARNING, "HRNP is REJECT");

                            //_errorHandler
                            Reconnect();

                            break;
                        case HRNP.OPCODE.CLOSE:
                            Logger.GetInstance().Add(Logger.Priority.WARNING, "HRNP is CLOSE");
                            //wyślij potwierdzenie rozłączenia
                            SendData(new HRNP(HRNP.OPCODE.CLOSE_ACK, _sourceID, _destinationID, null, hrnpPacket.PacketNumber));
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "CLOSE_ACK was sent");

                            //_errorHandler();
                            Reconnect();

                            break;
                        case HRNP.OPCODE.CLOSE_ACK:
                            break;
                        case HRNP.OPCODE.DATA:
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "HRNP is DATA");

                            //wyślij potwierdzenie przyjęcia danych
                            SendData(new HRNP(HRNP.OPCODE.DATA_ACK, _sourceID, _destinationID, null, hrnpPacket.PacketNumber));
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "DATA_ACK was sent");

                            //wykryj zawartość na podstawie poprawności wg protokołu
                            if ((new RCP(hrnpPacket.Payload)).IsDataValid)
                            {
                                callback(new RCP(hrnpPacket.Payload));
                            }
                            else if ((new TMP(hrnpPacket.Payload)).IsDataValid)
                            {
                                callback(new TMP(hrnpPacket.Payload));
                            }
                            else if ((new LP(hrnpPacket.Payload)).IsDataValid)
                            {
                                callback(new LP(hrnpPacket.Payload));
                            }
                            else
                            {
                                Logger.GetInstance().Add(Logger.Priority.WARNING, "HRNP payload is unknown");
                            }
                            break;
                        case HRNP.OPCODE.DATA_ACK:
                            Logger.GetInstance().Add(Logger.Priority.DEBUG, "HRNP is DATA_ACK");
                            break;
                        default:
                            break;
                    }
                }
            };
            _dataReader.Start();
        }

        public void SendData(HRNP hrnp)
        {
            if (_port.IsConnected)
            {
                Logger.GetInstance().Add("!Sending HRNP", hrnp.Data);
                _port.Write(hrnp.Data);
            }
            else
            {
                Logger.GetInstance().Add(Logger.Priority.DEBUG, "Packet cannot be transmitted");
            }
        }

        public void SendData(HDAP hdap)
        {
            SendData(new HRNP(HRNP.OPCODE.DATA, _sourceID, _destinationID, hdap.Data, _packetNumber++));
        }

        void ByteRead(byte b)
        {
            _serialBuffer.Add(b);

            if (_serialBuffer.Count >= 12)
            {
                //HRNP Pattern
                //7e [04|03] xx xx xx xx xx xx LH LL xx xx
                if (HRNP.PatternMatch(_serialBuffer.ToArray()))
                {
                    HRNP hrnpPacket = new HRNP(_serialBuffer.ToArray());
                    if (hrnpPacket.IsDataValid)
                    {
                        _rxQueue.Enqueue(hrnpPacket);
                        _serialBuffer.RemoveRange(0, hrnpPacket.Length);
                    }
                    else if (_serialBuffer.Count > hrnpPacket.Length)
                    {
                        _serialBuffer.RemoveAt(0);
                    }
                }
                else
                {
                    _serialBuffer.RemoveAt(0);
                }
            }

            while (_serialBuffer.Count > 512)
            {
                _serialBuffer.RemoveAt(0);
            }
        }

        void ClearRxQueue()
        {
            HRNP ignored;
            while (_rxQueue.TryDequeue(out ignored));
        }

        //public void Dispose()
        //{
        //    if (_dataReader != null)
        //    {
        //        _dataReader.Stop();
        //        _dataReader.Close();
        //        _dataReader.Dispose();
        //    }

        //    if (_port != null)
        //    {
        //        _port.Dispose();
        //    }
        //}
    }
}
