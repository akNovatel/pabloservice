﻿using PabloServiceCore.ConfigAccess;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace PabloServiceCore.Serial.Port
{
    class UARTPort : IPort
    {
        int _comNumber;
        SerialPort _serialPort = null;

        System.Timers.Timer _portWriter = null;

        bool _portListenerEnable = true;
        Task _portListener;

        ConcurrentQueue<byte[]> _writeBuffer = new ConcurrentQueue<byte[]>();

        const string _configContext = "uartport";

        public bool IsConnected { get { return _serialPort != null; } }
        IConfigAccess _config;

        public UARTPort(IConfigAccess config)
        {
            _config = config;

            _comNumber = _config.GetValue(_configContext, "com", 3);

            Connect();

            //PortWriter
            _portWriter = new System.Timers.Timer(64);
            _portWriter.Elapsed += (s, e) =>
            {
                byte[] data;
                if (_serialPort != null)
                {
                    if (_writeBuffer.TryDequeue(out data) && _serialPort.IsOpen)
                    {
                        _serialPort.Write(data, 0, data.Length);
                    }
                }
            };
            _portWriter.Start();
        }

        void Connect()
        {
            while (true)
            {
                _serialPort = OpenPort(_comNumber);
                if (_serialPort != null) { break; }
                Thread.Sleep(2000);
            }
        }

        SerialPort OpenPort(int com)
        {
            SerialPort serialPort;

            int baudrate = _config.GetValue(_configContext, "baudrate", 9600);
            int databits = _config.GetValue(_configContext, "databits", 8);

            Parity parity;
            switch (_config.GetValue(_configContext, "parity", "none"))
            {
                case "odd":
                    parity = Parity.Odd;
                    break;
                case "even":
                    parity = Parity.Even;
                    break;
                default:
                    parity = Parity.None;
                    break;
            }

            StopBits stopbits;
            switch (_config.GetValue(_configContext, "stopbits", 1))
            {
                case 1:
                    stopbits = StopBits.One;
                    break;
                case 2:
                    stopbits = StopBits.Two;
                    break;
                default:
                    stopbits = StopBits.None;
                    break;
            }

            try
            {
                serialPort = new SerialPort($"COM{com}", baudrate, parity, databits, stopbits);
                serialPort.Open();
                Logger.GetInstance().Add(Logger.Priority.INFO, $"UARTPort {serialPort.PortName} opened");

                return serialPort;
            }
            catch
            {
                return null;
            }
        }

        public void ListenPort(Action<byte> readCallback)
        {
            _portListener = new Task(() => 
            {
                while (_portListenerEnable)
                {
                    try
                    {
                        while (_portListenerEnable && _serialPort != null)
                        {
                            readCallback((byte)_serialPort.ReadByte());
                        }
                    }
                    catch
                    {
                        Logger.GetInstance().Add(Logger.Priority.ERROR, $"UARTPort COM{_comNumber} lost, reopening...");
                        //_errorHandler();
                        Connect();
                    }
                }
            });
            _portListener.Start();
        }

        public void Write(byte[] data)
        {
            _writeBuffer.Enqueue(data);
        }

        //public void Dispose()
        //{
        //    if (_portWriter != null)
        //    {
        //        _portWriter.Stop();
        //        _portWriter.Close();
        //        _portWriter.Dispose();
        //    }

        //    if (_serialPort != null)
        //    {
        //        _serialPort.Close();
        //        _serialPort.Dispose();
        //    }

        //    _portListenerEnable = false;
        //}
    }
}
