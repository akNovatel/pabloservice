﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Port
{
    interface IPort
    {
        bool IsConnected { get; }
        void Write(byte[] data);
        void ListenPort(Action<byte> readCallback);
        //void Reconnect();
    }
}
