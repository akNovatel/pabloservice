﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Message
{
    interface IMessage
    {
        UInt16 RadioID { get; }
        bool IsDataValid { get; }
    }
}
