﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Message
{
    public class GPS : HyteraMessage
    {
        public double Longtitude { get; private set; }
        public double Latitude { get; private set; }

        bool _dataSet = false;

        public GPS(byte[] data)
        {
            if (data.Length == 56)
            {
                RadioID = (UInt16)((data[6] << 8) | data[7]);
                double raw;

                raw = DecodeDoubleFromDec(data.Skip(30).Take(9).ToArray());
                Latitude = ((int)(raw / 100)) + ((raw % 100) / 60);

                raw = DecodeDoubleFromDec(data.Skip(40).Take(10).ToArray());
                Longtitude = ((int)(raw / 100)) + ((raw % 100) / 60);

                _dataSet = true;
            }
        }

        public override bool IsDataValid
        {
            get
            {
                if (_dataSet)
                {
                    return true;
                }
                return false;
            }
        }
    }
}
