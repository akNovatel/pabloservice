﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Message
{
    public abstract class HyteraMessage : IMessage
    {
        public abstract bool IsDataValid { get; }
        public UInt16 RadioID { get; protected set; }

        protected double DecodeDoubleFromDec(byte[] input)
        {
            int sum = 0;
            int exp = 0;
            //for (int i = input.Length - 1; i >= 0; i--)
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != 0x2e)
                {
                    int? nibble = DecodeNibble((char)input[i]);
                    if (nibble.HasValue)
                    {
                        sum += nibble.Value;
                        sum *= 10;
                    }
                }
                else
                {
                    exp = input.Length - i;
                }
            }
            return sum * Math.Pow(10, -exp);
        }

        protected int DecodeIntFromHex(byte[] input, int byteSkip = 1)
        {
            int sum = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (i % (byteSkip + 1) == 0)
                {
                    int? nibble = DecodeNibble((char)input[i]);
                    if (nibble.HasValue)
                    {
                        sum <<= 4;
                        sum |= nibble.Value;
                    }
                }
            }
            return sum;
        }

        protected int? DecodeNibble(char input)
        {
            if (input >= '0' && input <= '9')
            {
                return input & 0x0f;
            }
            else if (input >= 'A' && input <= 'F')
            {
                return input - 55;
            }
            else if (input >= 'a' && input <= 'f')
            {
                return input - 87;
            }
            return null;
        }
    }
}
