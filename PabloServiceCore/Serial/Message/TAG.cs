﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Serial.Message
{
    public class TAG : HyteraMessage
    {
        public UInt16 TagID { get; private set; }
        public byte RSSI { get; private set; }

        bool _dataSet = false;

        public TAG(byte[] data)
        {
            if (data.Length >= 34)
            {
                RadioID = (UInt16)((data[10] << 8) | data[11]);
                TagID = (UInt16)DecodeIntFromHex(data.Skip(12).Take(8).ToArray());
                RSSI = (byte)DecodeIntFromHex(data.Skip(22).Take(4).ToArray());
                _dataSet = true;
            }
        }

        public override bool IsDataValid
        {
            get
            {
                if (_dataSet)
                {
                    return RadioID > 0 && TagID > 0;
                }
                return false;
            }
        }
    }
}
