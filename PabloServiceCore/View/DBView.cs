﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.View
{
    public class DBView : IView
    {
        DBAccess.IDBAccess _dbAccess;

        enum ViewElement
        {
            TIME,
            NAME,
            TABLENAME,
            TABLEHEADER,
            TABLEBODY,
            LINE,
        }

        public enum Table
        {
            TAG,
            GPS,
            ALARM
        }
        //Table _table = Table.TAG;

        Dictionary<ViewElement, ConsoleColor> _elementColor = new Dictionary<ViewElement, ConsoleColor>
        {
            {ViewElement.TIME,           ConsoleColor.DarkCyan },
            {ViewElement.NAME,           ConsoleColor.DarkGreen },
            {ViewElement.TABLENAME,      ConsoleColor.Yellow },
            {ViewElement.TABLEHEADER,    ConsoleColor.White },
            {ViewElement.TABLEBODY,      ConsoleColor.DarkGray },
            {ViewElement.LINE,           ConsoleColor.Blue },
        };

        const int _columnWidth = 19;
        const int _consoleWidth = 74;
        const string timeFormat = "dd-MM-yy HH:mm:ss";

        public DBView(DBAccess.IDBAccess dbAccess)
        {
            _dbAccess = dbAccess;
        }

        public void Draw()
        {
            ConsoleColor cc = Console.ForegroundColor;
            Console.Clear();

            Console.ForegroundColor = _elementColor[ViewElement.TIME];
            Console.Write($"Time: {DateTime.Now.ToString(timeFormat), -48}");
            Console.ForegroundColor = _elementColor[ViewElement.NAME];
            Console.Write($"P.A.B.L.O. Service");
            Console.WriteLine();
            
            DrawLine(_consoleWidth);
            //switch (_table)
            //{
            //    case Table.TAG:
            //        DrawTable("tag");
            //        break;
            //    case Table.GPS:
            //        DrawTable("gps");
            //        break;
            //    case Table.ALARM:
            //        DrawTable("alarm");
            //        break;
            //    default:
            //        break;
            //}
            DrawTable("tag");
            DrawLine(_consoleWidth);
            DrawTable("gps");
            DrawLine(_consoleWidth);
            DrawTable("alarm");
            DrawLine(_consoleWidth);

            Console.ForegroundColor = cc;
        }

        void DrawLine(int length)
        {
            Console.ForegroundColor = _elementColor[ViewElement.LINE];
            string line = string.Join("-", (new string[length + 1]));
            Console.WriteLine(line);
        }

        void DrawTable(string table)
        {
            _dbAccess.OrderBy("id", DBAccess.Order.DESC);
            List<Dictionary<string, object>> records = _dbAccess.Get(table);

            if (records.Count > 0)
            {
                //nazwa
                Console.ForegroundColor = _elementColor[ViewElement.TABLENAME];
                Console.WriteLine($"{table.ToUpper()}:");

                //kolumny
                Console.ForegroundColor = _elementColor[ViewElement.TABLEHEADER];
                foreach (string column in records[0].Keys.ToArray().Skip(1))
                {
                    Console.Write(column.ToUpper().PadRight(_columnWidth));
                }
                Console.WriteLine();

                //wartości
                Console.ForegroundColor = _elementColor[ViewElement.TABLEBODY];
                foreach (var record in records)
                {
                    foreach (var item in record.Skip(1))
                    {
                        string value;
                        if (item.Key == "savetime" || item.Key == "tagtime")
                        {
                            DateTime dateTime = new DateTime(1970, 1, 1).AddSeconds(double.Parse(item.Value.ToString()));
                            value = dateTime.ToString(timeFormat);
                        }
                        else
                        {
                            value = item.Value.ToString();
                        }
                        Console.Write(value.PadRight(_columnWidth));
                    }
                    Console.WriteLine();
                }
            }
        }
    }
}
