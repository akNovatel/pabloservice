﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.View
{
    public interface IView
    {
        void Draw();
    }
}
