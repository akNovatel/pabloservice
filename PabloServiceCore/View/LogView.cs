﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.View
{
    class LogView : IView, ILogObserver
    {
        ConcurrentQueue<string> _logBuffer = new ConcurrentQueue<string>();

        public LogView()
        {
            Logger.GetInstance().Register(this);
        }

        public void Draw()
        {
            string log;

            while(_logBuffer.TryDequeue(out log))
            {
                Console.WriteLine(log);
            }
        }

        public void HandleLog(string log)
        {
            _logBuffer.Enqueue(log);
        }
    }
}
