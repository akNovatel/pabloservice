﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore
{
    public static class UnixTime
    {
        static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static long Get()
        {
            return (long)(DateTime.Now - UnixEpoch).TotalSeconds;
        }
    }
}
