﻿using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.ConfigAccess
{
    class YAMLAccess : IConfigAccess
    {
        Dictionary<string, object> _config;
        bool _extended = false;
        string _path;

        SerializerSettings _settings;

        public YAMLAccess(string filename)
        {
            _settings = new SerializerSettings();
            _settings.EmitAlias = false;
            _settings.EmitTags = false;
            _settings.SortKeyForMapping = false;

            _path = $"{AppDomain.CurrentDomain.BaseDirectory}/{filename}";
            try
            {
                string buffer = File.ReadAllText(_path);
                _config = new Serializer().Deserialize<Dictionary<string, object>>(buffer);
            }
            catch
            {
                Logger.GetInstance().Add(Logger.Priority.INFO, "YAMLAccess file read error");
                _config = new Dictionary<string, object>();
            }
        }

        public T GetValue<T>(string context, string key, T defval)
        {
            if (_config.ContainsKey(context))
            {
                if (_config[context] is IDictionary<object, object>)
                {
                    object value;
                    if((_config[context] as IDictionary<object, object>).TryGetValue(key, out value))
                    {
                        return (T)value;
                    }
                    else
                    {
                        (_config[context] as IDictionary<object, object>).Add(key, defval);
                    }
                }
            }
            else
            {
                _config.Add(context, new Dictionary<object, object>());
                (_config[context] as IDictionary<object, object>).Add(key, defval);
            }
            _extended = true;
            return defval;
        }

        public void SaveIfExtended()
        {
            if (_extended)
            {
                string buffer = new Serializer(_settings).Serialize(_config);
                File.WriteAllText(_path, buffer);
            }
        }
    }
}
