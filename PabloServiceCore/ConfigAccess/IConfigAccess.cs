﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.ConfigAccess
{
    public interface IConfigAccess
    {
        T GetValue<T>(string context, string key, T defval);
        void SaveIfExtended();
    }
}
