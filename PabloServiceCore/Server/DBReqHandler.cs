﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Server
{
    class DBReqHandler : IReqHandler
    {
        DBAccess.IDBAccess _dbAccess;

        public DBReqHandler(DBAccess.IDBAccess dbAccess)
        {
            _dbAccess = dbAccess;
        }

        bool IsAlnum(string s)
        {
            return s.All(c => char.IsLetterOrDigit(c));
        }

        //     JToken            - abstract base class
        //       JContainer      - abstract base class of JTokens that can contain other JTokens
        //           JArray      - represents a JSON array(contains an ordered list of JTokens)
        //           JObject     - represents a JSON object (contains a collection of JProperties)
        //           JProperty   - represents a JSON property(a name/JToken pair inside a JObject)
        //       JValue          - represents a primitive JSON value(string, number, boolean, null)
        void CriteriaParser(JToken input, string parent = null)
        {
            if (input is JContainer)
            {
                if (input is JArray)
                {
                    switch (parent)
                    {
                        case "$or":
                            _dbAccess.BeginGroup(DBAccess.Logic.OR);
                            break;
                        case "$and":
                            _dbAccess.BeginGroup(DBAccess.Logic.AND);
                            break;
                        default:
                            foreach (var item in input)
                            {
                                CriteriaParser(item);
                            }
                            return;
                    }

                    foreach (var element in input)
                    {
                        CriteriaParser(element);
                    }

                    _dbAccess.EndGroup();
                }
                else if (input is JObject)
                {
                    foreach (JProperty prop in input)
                    {
                        switch (prop.Name)
                        {
                            case "$lt":
                                _dbAccess.AddCriteria(parent, prop.Value, DBAccess.Compare.LT);
                                break;
                            case "$gt":
                                _dbAccess.AddCriteria(parent, prop.Value, DBAccess.Compare.GT);
                                break;
                            default:
                                CriteriaParser(prop.Value, prop.Name);
                                break;
                        }
                    }
                }
            }
            else if (input is JValue)
            {
                _dbAccess.AddCriteria(parent, input);
            }
        }

        public string Handler(string method, string primary, string secondary, NameValueCollection arguments)
        {
            if (primary == null)
            {
                return string.Empty;
            }

            try
            {
                if (secondary != null)
                {
                    _dbAccess.AddCriteria("id", secondary);
                }
                switch (method)
                {
                    case "GET":
                        if (arguments.AllKeys.Contains("q"))
                        {
                            var q = JsonConvert.DeserializeObject<JToken>(arguments["q"].Replace("\\", ""));
                            CriteriaParser(q);
                        }
                        if (arguments.AllKeys.Contains("sort"))
                        {
                            DBAccess.Order order = DBAccess.Order.ASC;
                            if (arguments.AllKeys.Contains("dir"))
                            {
                                if (arguments["dir"] == "-1")
                                {
                                    order = DBAccess.Order.DESC;
                                }
                            }
                            _dbAccess.OrderBy(arguments["sort"], order);
                        }
                        else
                        {
                            _dbAccess.OrderBy("id", DBAccess.Order.DESC);
                        }
                        if (arguments.AllKeys.Contains("max"))
                        {
                            int max;
                            if (int.TryParse(arguments["max"], out max))
                            {
                                if (arguments.AllKeys.Contains("skip"))
                                {
                                    int skip;
                                    if (int.TryParse(arguments["skip"], out skip))
                                    {
                                        _dbAccess.Limit(max, skip);
                                    }
                                }
                                else
                                {
                                    _dbAccess.Limit(max);
                                }
                            }
                        }
                        List<Dictionary<string, object>> records = _dbAccess.Get(primary);
                        return JsonConvert.SerializeObject(records);
                    case "POST":
                        Dictionary<string, object> data = new Dictionary<string, object>();
                        foreach (var key in arguments.AllKeys)
                        {
                            data.Add(key, arguments[key]);
                        }
                        if (secondary != null)
                        {
                            //UPDATE (id pozostanie z AddCriteria wywołanego przed switch)
                            _dbAccess.Update(primary, data);
                        }
                        else
                        {
                            //INSERT
                            _dbAccess.Put(primary, data);
                        }
                        return Handler("GET", primary, secondary, arguments);
                    default:
                        //response = "unsupported method";
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.GetInstance().Add(Logger.Priority.WARNING, $"DBRequestHandler: {e.Message}");
            }

            return string.Empty;
        }
    }
}
