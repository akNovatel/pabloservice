﻿using Newtonsoft.Json;
using PabloServiceCore.ConfigAccess;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PabloServiceCore.Server
{
    class HTTPServer
    {
        public int Port { get; private set; }

        HttpListener _listener;
        //Thread _server;

        //Func<string, string, string, NameValueCollection, string> _outsideFunc;

        const string _configContext = "httpserver";

        Dictionary<string, IReqHandler> _handlers = new Dictionary<string, IReqHandler>();

        //public HTTPServer(int port, Func<string, string, string, NameValueCollection, string> outsideFunc)
        public HTTPServer(IConfigAccess config)
        {
            Port = config.GetValue(_configContext, "port", 45053);
            //_outsideFunc = reqHandler.Handler;

            //_server = new Thread(Listen);
            //_server.Start();
            new Thread(Listen).Start();
        }

        //public void AddHandler(string context, Func<string, string, string, NameValueCollection, string> extFunc)
        public void AddHandler(string context, IReqHandler handler)
        {
            _handlers.Add(context, handler);
        }

        IReqHandler GetHandler(string context)
        {
            if (_handlers.ContainsKey(context))
            {
                return _handlers[context];
            }
            Logger.GetInstance().Add(Logger.Priority.WARNING, "HTTPServer: Unsupported context");

            //zwróć dummy handler;
            return new NullReqHandler();
        }

        void Listen()
        {
            _listener = new HttpListener();
            _listener.Prefixes.Add($"http://*:{Port}/");
            _listener.Start();

            while (true)
            {
                Process(_listener.GetContext());
            }
        }

        void Process(HttpListenerContext context)
        {
            string path = context.Request.Url.AbsolutePath;

            //jeśli bez ścieżki lub tylko ze slashem to wyjdź
            if (path.Length < 2)
            {
                return;
            }

            path = path.Substring(1); //obcięcie pierwszego '/'
            if(path[path.Length - 1] == '/') { path = path.Substring(0, path.Length - 1); } //obcjęcie ewentualnego ostatniego '/'
            string[] pathElements = path.Split('/');

            string primary;
            if (pathElements.Length < 2)
            {
                primary = null;
            }
            else
            {
                primary = pathElements[1];
            }

            string secondary;
            if (pathElements.Length < 3)
            {
                secondary = null;
            }
            else
            {
                secondary = pathElements[2];
            }

            string handler = pathElements[0];

            NameValueCollection arguments = new NameValueCollection();
            switch (context.Request.HttpMethod)
            {
                case "GET":
                    arguments = context.Request.QueryString;
                    break;
                case "POST":
                    Dictionary<string, string> argumentDict = JsonConvert.DeserializeObject<Dictionary<string, string>>((new StreamReader(context.Request.InputStream)).ReadToEnd());
                    foreach (var argument in argumentDict)
                    {
                        arguments.Add(argument.Key, argument.Value);
                    }
                    break;
            }

            byte[] body = Encoding.Default.GetBytes(GetHandler(handler).Handler(context.Request.HttpMethod, primary, secondary, arguments));
            Response(context, body, "application/json");
        }

        void Response(HttpListenerContext context, byte[] input, string mime)
        {
            context.Response.ContentType = mime;
            context.Response.ContentLength64 = input.Length;
            context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
            context.Response.AddHeader("Last-Modified", DateTime.Now.ToString("r"));

            context.Response.OutputStream.Write(input, 0, input.Length);

            context.Response.StatusCode = (int)HttpStatusCode.OK;

            context.Response.OutputStream.Flush();
            context.Response.OutputStream.Close();
        }
    }
}
