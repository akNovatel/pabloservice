﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.Server
{
    public interface IReqHandler
    {
        string Handler(string method, string primary, string secondary, NameValueCollection arguments);
    }
}
