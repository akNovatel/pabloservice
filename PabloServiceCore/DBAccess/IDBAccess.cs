﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.DBAccess
{
    public enum ColumnType
    {
        VARCHAR,
        INT
    }

    public enum Order
    {
        ASC,
        DESC
    }

    public enum Compare
    {
        EQ,
        GT,
        LT
    }

    public enum Logic
    {
        AND,
        OR
    }

    //public struct Criteria
    //{
    //    public string column;
    //    public string value;
    //    public Compare compare;
    //}

    public interface IDBAccess
    {
        void Close();
        void AddCriteria(string column, object value, Compare compare = Compare.EQ, Logic logic = Logic.AND);
        void BeginGroup(Logic logic = Logic.OR);
        void EndGroup();
        void OrderBy(string column, Order order);
        void Limit(int limit, int skip = 0);
        void Put(string table, Dictionary<string, object> data);
        List<Dictionary<string, object>> Get(string table, string[] columns = null);
        void Remove(string table);
        void Update(string table, Dictionary<string, object> data);
        void UpdateExisting(string table, Dictionary<string, object> data);
        void Create(string table, Dictionary<string, ColumnType> columns);
        bool Exists(string table);
        int Count(string table);

        void AddVirtual(string table, string query);
    }
}
