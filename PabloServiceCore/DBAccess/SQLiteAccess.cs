﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PabloServiceCore.DBAccess
{
    class SQLiteAccess : IDBAccess
    {
        SQLiteConnection _connection;

        string _criteria = string.Empty;
        string _order = string.Empty;
        string _limit = string.Empty;

        bool _groupStarted = false;
        //bool _inGroup = false;
        //Logic _groupLogic = Logic.AND;

        List<Logic> _groupLogicStack = new List<Logic>();

        Dictionary<string, string> _virtualTables = new Dictionary<string, string>();

        public SQLiteAccess(string filename)
        {
            if (!File.Exists(filename))
            {
                SQLiteConnection.CreateFile(filename);
            }

            _connection = new SQLiteConnection($"Data Source={filename};Version=3;");
            _connection.Open();
        }

        public void Close()
        {
            _connection.Close();
        }

        string ToQuotes(object[] elements)
        {
            return $"'{string.Join("', '", elements)}'";
        }

        void StartCriteria(Logic logic = Logic.AND)
        {
            if (_criteria != string.Empty)
            {
                switch (logic)
                {
                    case Logic.AND:
                        _criteria = $"{_criteria} AND";
                        break;
                    case Logic.OR:
                        _criteria = $"{_criteria} OR";
                        break;
                    default:
                        break;
                }
            }
            else
            {
                _criteria = $"WHERE";
            }
        }

        public void AddCriteria(string column, object value, Compare compare = Compare.EQ, Logic logic = Logic.AND)
        {
            if (_groupStarted == false)
            {
                if (_groupLogicStack.Count > 0)
                {
                    StartCriteria(_groupLogicStack.Last());
                }
                else
                {
                    StartCriteria(logic);
                }
            }

            switch (compare)
            {
                case Compare.EQ:
                    _criteria = $"{_criteria} {column}='{value}'";
                    break;
                case Compare.GT:
                    _criteria = $"{_criteria} {column}>'{value}'";
                    break;
                case Compare.LT:
                    _criteria = $"{_criteria} {column}<'{value}'";
                    break;
                default:
                    break;
            }
            _groupStarted = false;
        }

        public void BeginGroup(Logic logic)
        {
            if (_criteria == string.Empty)
            {
                _criteria = $"WHERE";
            }
            if (_groupLogicStack.Count > 0 && _groupStarted == false)
            {
                StartCriteria(_groupLogicStack.Last());
            }
            _criteria = $"{_criteria} (";
            _groupStarted = true;
            _groupLogicStack.Add(logic);
        }

        public void EndGroup()
        {
            _criteria = $"{_criteria} )";
            _groupLogicStack.Remove(_groupLogicStack.Last());
        }

        public void OrderBy(string column, Order order)
        {
            _order = $"ORDER BY {column} ";
            switch (order)
            {
                case Order.ASC:
                    _order += "ASC";
                    break;
                case Order.DESC:
                    _order += "DESC";
                    break;
                default:
                    break;
            }
        }

        void ApplyModifiers(ref string query)
        {
            if (_criteria != string.Empty)
            {
                query = $"{query} {_criteria}";
                _criteria = string.Empty;
            }

            if (_order != string.Empty)
            {
                query = $"{query} {_order}";
                _order = string.Empty;
            }

            if (_limit != string.Empty)
            {
                query = $"{query} {_limit}";
                _limit = string.Empty;
            }
        }

        public void Put(string table, Dictionary<string, object> data)
        {
            string query = $"INSERT INTO {table} ({ToQuotes(data.Keys.ToArray())}) VALUES ({ToQuotes(data.Values.ToArray())})";
            (new SQLiteCommand(query, _connection)).ExecuteNonQuery();
        }

        public List<Dictionary<string, object>> Get(string tableName, string[] columns = null)
        {
            string table = tableName;
            if (_virtualTables.ContainsKey(table))
            {
                table = $"({_virtualTables[table]})";
            }

            string query;
            if (columns == null)
            {
                query = $"SELECT * FROM {table}";
            }
            else
            {
                query = $"SELECT {string.Join(",", columns)} FROM {table}";
            }
            ApplyModifiers(ref query);

            //Console.WriteLine(query);
            SQLiteCommand command = new SQLiteCommand(query, _connection);
            SQLiteDataReader reader = command.ExecuteReader();

            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            while (reader.Read())
            {
                Dictionary<string, object> dict = new Dictionary<string, object>();
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    string column = reader.GetName(i);
                    dict.Add(column, reader[column]);
                }
                list.Add(dict);
            }
            return list;

            //Console.WriteLine(query);
            //return new List<Dictionary<string, object>>();
        }

        public void Remove(string table)
        {
            string query = $"DELETE FROM {table}";
            ApplyModifiers(ref query);
            (new SQLiteCommand(query, _connection)).ExecuteNonQuery();
        }

        public void Update(string table, Dictionary<string, object> data)
        {
            string[] nameValue = data.Select((item) => $"{item.Key} = '{item.Value}'").ToArray();
            string query = $"UPDATE {table} SET {string.Join(", ", nameValue)}";
            ApplyModifiers(ref query);
            (new SQLiteCommand(query, _connection)).ExecuteNonQuery();
        }

        public void Create(string table, Dictionary<string, ColumnType> columns)
        {
            List<string> props = new List<string>();
            foreach (var column in columns)
            {
                string type = string.Empty;
                switch (column.Value)
                {
                    case ColumnType.VARCHAR:
                        type = "VARCHAR";
                        break;
                    case ColumnType.INT:
                        type = "INT";
                        break;
                    default:
                        break;
                }
                props.Add($"{column.Key} {type}");
            }

            string query = $"CREATE TABLE IF NOT EXISTS {table} (id INTEGER PRIMARY KEY AUTOINCREMENT, {string.Join(", ", props)})";
            (new SQLiteCommand(query, _connection)).ExecuteNonQuery();
        }

        public bool Exists(string table)
        {
            AddCriteria("type", "table");
            AddCriteria("name", table);
            return Get("sqlite_master").Count > 0;
        }

        public int Count(string table)
        {
            string query = $"SELECT COUNT(*) FROM {table}";
            ApplyModifiers(ref query);

            SQLiteCommand command = new SQLiteCommand(query, _connection);
            SQLiteDataReader reader = command.ExecuteReader();

            reader.Read();
            return reader.GetInt32(0);
        }

        public void UpdateExisting(string table, Dictionary<string, object> data)
        {
            List<Dictionary<string, object>> results = Get(table);
            if (results.Count > 0)
            {
                AddCriteria("id", results[0]["id"]);
                Update(table, data);
            }
            else
            {
                Put(table, data);
            }
        }

        public void Limit(int limit, int skip = 0)
        {
            if (skip > 0)
            {
                _limit = $"LIMIT {skip},{limit}";
            }
            else
            {
                _limit = $"LIMIT {limit}";
            }
        }

        public void AddVirtual(string table, string query)
        {
            _virtualTables.Add(table, query);
        }
    }
}
