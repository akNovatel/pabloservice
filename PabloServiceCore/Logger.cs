﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using PabloServiceCore.ConfigAccess;

namespace PabloServiceCore
{
    public interface ILogObserver
    {
        void HandleLog(string log);
    }

    public class Logger
    {
        static Logger _instance = null;

        ConcurrentQueue<string> _logBuffer = new ConcurrentQueue<string>();

        List<ILogObserver> _observers = new List<ILogObserver>();

        Priority _priority = Priority.ERROR;

        const string _filename = "PabloService.log";

        public enum Priority
        {
            NONE,
            ERROR,
            WARNING,
            INFO,
            DEBUG
        }

        public static Logger GetInstance()
        {
            if (_instance == null)
            {
                _instance = new Logger();
            }
            return _instance;
        }

        internal void Setup(IConfigAccess config)
        {
            switch (config.GetValue("logging", "level", "error"))
            {
                case "none":
                    _priority = Priority.NONE;
                    break;
                case "error":
                    _priority = Priority.ERROR;
                    break;
                case "warning":
                    _priority = Priority.WARNING;
                    break;
                case "info":
                    _priority = Priority.INFO;
                    break;
                case "debug":
                    _priority = Priority.DEBUG;
                    break;
                default:
                    break;
            }
        }

        private Logger()
        {
            ////wyczyść plik
            //File.WriteAllText(_filename, string.Empty);

            //log writer z kolejki
            System.Timers.Timer logWriter = new System.Timers.Timer(256);
            logWriter.Elapsed += (s, e) =>
            {
                string log;
                while (_logBuffer.TryDequeue(out log))
                {
                    File.AppendAllText(_filename, $"{log}\n");
                }
            };
            logWriter.Start();

            //log monitor usuwajacy najstarsze wpisy

        }

        public void Register(ILogObserver observer)
        {
            _observers.Add(observer);
        }

        bool PriorityChecker(Priority p)
        {
            if (_priority == Priority.NONE)
            {
                return false;
            }

            if (p == Priority.ERROR)
            {
                return true;
            }

            if (p == Priority.WARNING && (_priority == Priority.WARNING || _priority == Priority.INFO || _priority == Priority.DEBUG))
            {
                return true;
            }

            if (p == Priority.INFO && (_priority == Priority.INFO || _priority == Priority.DEBUG))
            {
                return true;
            }

            if (p == Priority.DEBUG && _priority == Priority.DEBUG)
            {
                return true;
            }

            return false;
        }

        public void Add(Priority p, string log)
        {
            if (PriorityChecker(p))
            {
                //File.AppendAllText(_filename, $"{DateTime.Now.ToString()} {log}\n");
                _logBuffer.Enqueue($"{DateTime.Now.ToString()} {log}");

                foreach (var observer in _observers)
                {
                    observer.HandleLog(log);
                }
            }
        }

        public void Add(string name, byte[] bytes)
        {
            string buffer = $"{name}: ";
            foreach (var item in bytes)
            {
                buffer += $"{item.ToString("X2")} ";
            }
            Add(Priority.DEBUG, buffer);
        }

        public void Wait()
        {
            while(_logBuffer.Count > 0) { Thread.Sleep(32); }
        }
    }
}
